<?php
/**
 * Template Name: Films Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cinema
 */

get_header(); ?>

	  
<section class="main__content"   data-aos="fade-up" data-aos-duration=1000" data-aos-delay="800">

		<?php the_content(); ?> 

</section>
<section class="section__movies">
	<div class="container-fluid">
		<div class="row row__bg--white">
			<div class="col-c">
				<div class="row d-flex justify-content-lg-start justify-content-sm-center">
					<div class="movies__box">
						<div class="movies__search">
						<?php 
							$section__nav = get_field('all_post');
						?>
							<form id="search-movie" method="post">
								<input type="text" name="movie" id="movie" placeholder="<?php echo esc_attr($section__nav['tab_title'],'cinema'); ?>"> 
								<input type="submit" name="submit-movie" value="search" id="submit-movie"> 
							</form>	
						</div>
					</div>
					<div class="movies__box">
			
						<div class="movies__title active"><a href="#" id="movies-all" class="movies__link"><?php _e($section__nav['tab_title'],'cinema'); ?></a></div>
					</div>				
				</div>

			</div>
		</div>
	</div>
	<div class="container-fluid all-film"  data-aos="fade-left" data-aos-duration=1000" data-aos-delay="500">
		<div class="row">
			<div class="col-lg-12">
					<div class="movies__content d-flex justify-content-between align-content-align-content-start ">
						<div class="content__inline--start">
						<?php
						$featured_img = $section__nav['tab_featured_img'];
				
						if ( $featured_img ) {
							$img_s = wp_get_attachment_image(
								$featured_img['id'],
								'section',
								false,
								array(
									'class' => 'film__pic',
									'title' => $featured_img['title'],
									'alt'   => $featured_img['alt'],
								)
							);
							echo $img_s;
						} ?>
						</div>

						<div class="content__inline--end">
						<?php
							$secondary_img = $section__nav['tab_secondary_img'];
							
							if ( $secondary_img ) {
								$img_e = wp_get_attachment_image(
									$secondary_img['id'],
									'section',
									false,
									array(
										'class' => 'film__pic',
										'title' => $secondary_img['title'],
										'alt'   => $secondary_img['alt'],
									)
								);
								echo $img_e;
							} ?>
						</div>
					
					</div>		

				</div>	
			</div>
	
		<div class="row movies__container">
			<div class="col-lg-12">
				<h2 class="block__title"><?php _e($section__nav['caption'],'cinema'); ?></h2>
			</div>
			<div class="col-lg-12">
			<?php 
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					 
				$wp_query = new WP_Query( array(
					'paged'			   => $paged,
					'post_type'        => 'films',                 
					'post_status'      => 'publish', 
					'posts_per_page'   => $section__nav['count_post'],
					'orderby' => 'ID',
					'order' => 'ASC'
				)); ?>
		
				<ul class="movies__list"> 
				<?php
				$delay = 300;
					while ( $wp_query->have_posts() ) { 
						$wp_query->the_post();
						$film = get_field('section_film');
						$delay =+ 500;				
					?>
					<li class="movies__item"  data-aos="fade-left" data-aos-duration=1000" data-aos-delay="<?php echo $delay; ?>">
						<div class="movies__item--wrap">
							<div class="movies__img">
							<?php
							if ($film['show_img']) {
								$film_img = $film['img'];
								if ( $film_img ) {
									$img_f = wp_get_attachment_image(
									$film_img,
									'section',
									false,
									array(
										'class' => 'film__imgc',
										'title' => $film_img['title'],
										'alt'   => $film_img['alt'],
									)
								);
								echo $img_f;
								}
							}
							if ($film['show_video']) {
								echo $film['video']; 
							} ?>
							</div>
							<div class="movies__info">
								<div class="movies__info--header">
									<div class="movies__title">
										<?php _e($film['title'],'cinema'); ?>
									</div>
								</div>
								<div class="movies__info--except">
									<?php echo wp_strip_all_tags($film['except'],'<p>'); ?>
								</div>
								<div class="movies__info--footer">
									<div class="info__inline--start">
										<?php _e($film['rating'],'cinema'); ?>
									</div>
									<div class="info__inline--end">
										<a class="btn btn-alt"
											href="<?php echo esc_url($film['link']['url']); ?>" 
											target="<?php esc_attr_e($film['link']['target'], '_s' ); ?>"
											title="<?php _e($film['link']['title']); ?>">
											<?php _e($film['link']['title']); ?>
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<?php 	}	wp_reset_postdata(); ?>
				</ul>	
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="pigination d-flex flex-justify-center align-items-center">
					<?php
					$big = 999999999; // need an unlikely integer
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages,
						'show_all'     => False,
						'end_size'     => 0,
						'mid_size'     => 1,
						'prev_next'    => True,
						'next_text' => __('הבא'),
						'prev_text' => __('קודם'),
						'type'         => 'plain',
						'add_args'     => False,
						'add_fragment' => '',
						'before_page_number' => '',
						'after_page_number'  => ''
					) ); ?>
		
				</div>
			</div>
		</div>	
					</div>	
	<div class="container-fluid result">
		<div class="row">
			<div class="col-lg-12">
				<ul class="movies__list" id="searchResult">
				</ul>
			</div>
		</div>
	</div>
</section>

<?php 
get_footer();