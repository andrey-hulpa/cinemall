<?php    
/*        Template Layout Header Video         */
?>

<!--   Template Layout Header Video     -->   
<div class="header__banner-wrap">
	<div class="box__shadow"></div>
	<?php
		$video_section =  get_field('section_header')['section_video_local'];
		$src_mp4 = $video_section['video_mp4'];
		$src_webm = $video_section['video_webm'];
		$poster = $video_section['video_poster'];
		$poster_sm = $video_section['video_poster_mobile'];
		$_style = 'background: url('. esc_url( $poster['url'] ) .') #000 no-repeat center center absolute; min-width: 100%;   min-height: 100%; width: auto; height: auto; z-index: -100; transition: 1s opacity; @media screen and (max-device-width: 767px) { .banner__video-bg{ background: url('. esc_url( $poster_sm['url']  ) .') #000 no-repeat center center fixed; } #video-h-banner { display: none; } }';
		 ?>
			<div class="header__banner banner__video-bg overlay d-flex align-items-center" style="<?php echo $_style; ?>">
			<video 	
			<?php if ($poster) : ?> poster="<?php echo esc_attr($poster['url']); ?>" <?php endif; ?> 
				id="video-h-banner" 
				playsinline autoplay muted loop >
				<?php if ($src_webm) : ?><source src="<?php echo esc_attr($src_webm); ?>" type="video/webm"> <?php endif; ?>
				<?php if ($src_mp4) : ?><source src="<?php echo esc_attr($src_mp4); ?>" type="video/mp4">  <?php endif; ?>

			</video>
		<?php 
		if (get_field('show_banner_content')) {
			$header_content = get_field('banner_content');
			?>
			<div class="banner__row">
				<h1 class="title__h1" data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
					<?php _e($header_content['title']); ?>
					<span class="shadow"><?php _e($header_content['title'], 'cinema');?></span>
				</h1>
				<?php if ($header_content['content']) { ?>
					<div class="banner__content"  data-aos="fade-left" data-aos-duration="800" data-aos-delay="800">
						<?php _e($header_content['title'], 'cinema');?>
					</div>
				<?php } ?>

				<ul class="buttons__list">
					<?php 
					$delay = 500;
					$buttons = $header_content['buttons'];
					foreach ($buttons as $button) : 
						$delay +=300; ?>
						<li class="button__item" data-aos="zoom-in" data-aos-duration="800" data-aos-delay="<?php echo $delay; ?>">
							<a href="<?php echo esc_url($button['link_button']);?>" class="btn"><h3 class="h3"><?php _e($button['caption_button'], 'cinema');?></h3></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php }	?>
	</div>
</div>
<!--   Template Layout Header Video    -->   
