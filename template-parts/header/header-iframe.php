<?php    
/*        Template Layout Header Iframe         */
?>

<!--   Template Layout Header Iframe     -->   
<div class="header__banner-wrap">
	<div class="box__shadow"></div>
	<?php
		$video_section =  get_field('section_header')['section_video_iframe'];
		$iframe = $video_section['video_iframe'];
		preg_match('/src="(.+?)"/', $iframe, $matches);
		$src = $matches[1];
		$src = substr($src, 0, strpos($src, '?'));
	
		?>	
		<div class="header__banner banner__video-bg banner__iframe-bg video-box overlay d-flex align-items-center"> 

		 	<iframe class="header_video_banner" 
				src="<?php echo $src; ?>?background=1&autoplay=1&loop=1&byline=0&title=0">
			</iframe> 
		<?php 
		if (get_field('show_banner_content')) {
			$header_content = get_field('banner_content');
			?>
			<div class="banner__row video-content">

				<h1 class="title__h1" data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
					<?php _e($header_content['title']); ?>
					<span class="shadow"><?php _e($header_content['title'], 'cinema');?></span>

				</h1>
				<?php if ($header_content['content']) { ?>
					<div class="banner__content"  data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
						<?php _e($header_content['title'], 'cinema');?>
					</div>
				<?php } ?>

				<ul class="buttons__list">

					<?php 
					$delay = 500;
					$buttons = $header_content['buttons'];
					foreach ($buttons as $button) : 
						$delay +=300; ?>
						<li class="button__item" data-aos="zoom-in" data-aos-duration="800" data-aos-delay="<?php echo $delay; ?>">
							<a href="<?php echo esc_url($button['link_button']);?>" class="btn"><h3 class="h3"><?php _e($button['caption_button'], 'cinema');?></h3></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php }	?>
	</div>
</div>
<!--   Template Layout Header Iframe    -->   
