<?php    
/*        Template Layout Header Image         */
?>

<!--   Template Layout Header Image     -->   
<div class="header__banner-wrap">
	<div class="box__shadow"></div>
	<?php
		$header 			= get_field('section_header')['section_img'];
		$header_img 		= $header['img']['url'];
		$header_img_mobile  = $header['img_mobile']['url'] ? $header['img_mobile']['url'] : $header_img;
		$_style =  'background: url('. esc_url( $header_img  ) . ') no-repeat center center; @media (max-width: 767px){.banner__img-bg{ background: url('. esc_url( $header_img_mobile  ) . ') #000 no-repeat center center fixed;}}';    ?>
		<div class="header__banner banner__img-bg overlay d-flex align-items-center" style="<?php echo esc_attr($_style); ?> ">
		<?php
		if (get_field('show_banner_content')) {
			$header_content = get_field('banner_content');
			?>
			<div class="banner__row">
				<h1 class="title__h1" data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
					<?php _e($header_content['title']); ?>
					<span class="shadow"><?php _e($header_content['title'], 'cinema');?></span>

				</h1>
				<?php if ($header_content['content']) { ?>
					<div class="banner__content"  data-aos="fade-left" data-aos-duration="1000" data-aos-delay="800">
						<?php _e($header_content['title'], 'cinema');?>
					</div>
				<?php } ?>
				<ul class="buttons__list">
					<?php 
					$delay = 200;
					$buttons = $header_content['buttons'];
					foreach ($buttons as $button) : 
						$delay +=300; ?>
						<li class="button__item" data-aos="zoom-in" data-aos-duration="800" data-aos-delay="<?php echo $delay; ?>">
							<a href="<?php echo esc_url($button['link_button']);?>" class="btn"><h3 class="h3"><?php _e($button['caption_button'], 'cinema');?></h3></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php }	?>
	</div>
</div>
<!--   Template Layout Header Image     -->   
