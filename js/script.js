$(document).ready(function(){
	$('#search-movie').on('submit', function (e) {
    e.preventDefault();
    var s = $('#movie').val();
    var data = {
        'action': 'filmfilter',
        'query': s 
    };
    $.ajax({
        url: '/wp-admin/admin-ajax.php',
        data: data,
        type:'POST',
        beforeSend: function( xhr){
          $('#searchResult').empty();
        },
        success:function(data){
          if (data) {
            console.log(data);
            $('.all-film').hide();
            $('.result').show();
            $('#searchResult').append(data);
          }
       }
      });
  });

  $('#movies-all').on('click', function (e) {
    e.preventDefault();
    AOS.refresh();
    $('.all-film').show();
    $('#movie').val('');
    $('.result').hide();
  });

  AOS.init();

 $(".lazy").Lazy();

  if ($('#preloader')) {
    $(window).load(function() { 
        $('#status').fadeOut(); 
        $('#preloader').delay(1500).fadeOut('slow'); 
    })
  }


	$('.nav__img-bars').on('click', function () {
		$('.header__nav').addClass('show-menu');
		$('.nav__img-bars').hide();			
		$('.nav__img-close').show();

	});	

	$('.nav__img-close').on('click', function () {
		$('.header__nav').removeClass('show-menu');
		$('.nav__img-bars').show();
		$('.nav__img-close').hide();

	});

	$( '#last__title','#last__title1').click(function (e) {
		e.preventDefault();
		$('body,html').animate({
		    scrollTop: 0
		},1000);
		return false;
	});
  
 function setVideoCenter() {

  var $box = $('.video-box');
  var height = $box.height();
  var width = $box.width();
  var new_height = width / 1.78;

  if (new_height > height) {

    $box.find('iframe').css({
      width: width, 
      height: new_height, 
      top: '-' + (new_height / 2 - height / 2) + 'px',
      left: '0',
    }); 

  } else {
    var new_width = height * 1.78;
    $box.find('iframe').css({

      width: new_width, 
      height: height, 
      top: '0',
      left: '-' + (new_width / 2 - width / 2) + 'px'
    });
    
  } 
}
  $(function(){
    setVideoCenter();
    $(window).resize(setVideoCenter);
  });

});

								








  ( function() {
    if (document.getElementById('featured__slider')) {

   new Vue({
    el: '#featured__slider',
    data: {
     // slides:  3,
   	display: 3 ,
    },
    components: {
      'carousel-3d': window['carousel-3d'].Carousel3d,
      'slide': window['carousel-3d'].Slide
    },
   
  });
}
if (document.getElementById('featured__slider')) {
  
 new Vue({
  el: '#films__slider',
  data: {
   // slides:  3,
 	display: 3 ,
  },
  components: {
    'carousel-3d': window['carousel-3d'].Carousel3d,
    'slide': window['carousel-3d'].Slide
  },
});
}
if (document.getElementById('featured__slider')) {

 new Vue({
  el: '#eat__slider',
  data: {
   // slides:  3,
 	display: 3 ,
  },
  components: {
    'carousel-3d': window['carousel-3d'].Carousel3d,
    'slide': window['carousel-3d'].Slide
  },
}); 
}
if (document.getElementById('featured__slider')) {


 new Vue({
  el: '#shop__slider',
  data: {
   // slides:  3,
 	display: 3 ,
  },
  components: {
    'carousel-3d': window['carousel-3d'].Carousel3d,
    'slide': window['carousel-3d'].Slide
  },
});
}

 
})();