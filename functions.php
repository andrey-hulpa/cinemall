<?php
/**
 * cinema functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cinema
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'cinema_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cinema_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on cinema, use a find and replace
		 * to change 'cinema' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cinema', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-top-bar'  => esc_html__( 'Top-bar', 'cinema' ),
				'main-menu-right' => esc_html__( 'Header-right', 'cinema' ),
				'main-menu-left' => esc_html__( 'Header-left', 'cinema' ),
				)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'cinema_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'cinema_setup' );

add_filter( 'mime_types', 'aco_extend_mime_types' );

function aco_extend_mime_types( $existing_mimes ) {

  // Add webm, mp4 and OGG to the list of mime types
  $existing_mimes['webm'] = 'video/webm';
  $existing_mimes['mp4']  = 'video/mp4';
  $existing_mimes['ogg']  = 'video/ogg';

  // Return an array now including our added mime types
  return $existing_mimes;
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cinema_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'cinema_content_width', 640 );
}
add_action( 'after_setup_theme', 'cinema_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cinema_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'cinema' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'cinema' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'cinema_widgets_init' );
remove_filter('the_content', 'wpautop');
/**
 * Enqueue scripts and styles.
 */
function cinema_scripts() {


	wp_enqueue_script( 'cinema-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	// 	if ( ! is_admin() ) {
	// 	wp_deregister_script( 'jquery' );
	// 	wp_register_script( 'jquery', ( 'https://code.jquery.com/jquery-3.4.1.min.js' ), false, '3.4.1', true );
	// 	wp_enqueue_script( 'jquery' );
	// }
	
	wp_enqueue_script( 'jQuery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(),  true );


	//wp_enqueue_script( 'jQuery', 'http://code.jquery.com/jquery-1.11.0.min.js', array(),  true );
  	wp_enqueue_script( 'jquery-migrate', 'http://code.jquery.com/jquery-migrate-1.2.1.min.js', array(),  true );
  	wp_enqueue_style( 'Font-Awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', true );

	wp_enqueue_script( 'jquery-lazy', get_template_directory_uri() . '/js/jquery.lazy.min.js', 'jquery', '1.7.10', true );

	wp_enqueue_style( 'aos-css', get_template_directory_uri() . '/css/aos.css', 'cinema-style', '2.3.1' );
	wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/js/aos.js', 'cinema-script', '2.3.1', true );


	wp_enqueue_script( '_s-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	// wp_enqueue_style( 'swiper-css', get_template_directory_uri() . '/css/swiper.min.css', true );
	// wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/js/swiper.min.js', 'jquery', _S_VERSION, true );
	wp_enqueue_script( 'vue-js', get_template_directory_uri() . '/js/vue.js', '', _S_VERSION, true );
	wp_enqueue_script( 'carousel-3d', get_template_directory_uri() . '/js/carousel-3d.umd.js', '', _S_VERSION, true );

	wp_enqueue_style( 'cinema-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'custom-style', get_template_directory_uri() .'/custom.css','', '1.0.0' );
	wp_style_add_data( 'cinema-style', 'rtl', 'replace' );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
		wp_enqueue_script( '_custom-js', get_template_directory_uri() . '/js/script.js', 'jquery' ,_S_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'cinema_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

## Отключает Гутенберг (новый редактор блоков в WordPress).
## ver: 1.2
if( 'disable_gutenberg' ){
	remove_theme_support( 'core-block-patterns' ); // WP 5.5

	add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );

	// отключим подключение базовых css стилей для блоков
	// ВАЖНО! когда выйдут виджеты на блоках или что-то еще, эту строку нужно будет комментировать
	remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );

	// Move the Privacy Policy help notice back under the title field.
	add_action( 'admin_init', function(){
		remove_action( 'admin_notices', [ 'WP_Privacy_Policy_Content', 'notice' ] );
		add_action( 'edit_form_after_title', [ 'WP_Privacy_Policy_Content', 'notice' ] );
	} );
}

if ( function_exists( 'acf_add_options_page' ) ) {
	$option_page = acf_add_options_page( array(
		'page_title' => 'Theme Global Settings',
		'menu_title' => 'Global Settings',
		'menu_slug'  => 'theme-global-settings',
		'capability' => 'edit_posts',
		'redirect'   => false
	) );
}



function cinemall_ajax_film_filter(){
	$query = $_POST['query'];
	$args = array(
		'posts_per_page' => -1,
		'post_type' => 'films',
		'post_status' => 'publish', 
	//	'orderby' => 'title',
	//	'order' => 'DESC',
		's' => $query,
		'sentence' => 1
	);
	
	$films = new WP_Query( $args );
	$delay = 300;
	if( $films->have_posts()){
		while ( $films->have_posts() ) { 
			$films->the_post();
			$post_id = $films->post->ID;
			$film = get_field('section_film', 	$post_id );
			$delay =+ 500;
			?>
			<li class="movies__item" data-aos="fade-left" data-aos-duration=1000" data-aos-delay="<?php echo $delay; ?>"> 
				<div class="movies__item--wrap">
					<div class="movies__img">
						<?php
						if ($film['show_img']) {
							$film_img = $film['img'];
							if ( $film_img ) {
								$img_f = wp_get_attachment_image(
								$film_img,
								'section',
								false,
								array(
									'class' => 'film__imgc',
									'title' => $film_img['title'],
									'alt'   => $film_img['alt'],
								)
							);
							echo $img_f;
							}
						}
						if ($film['show_video']) {
							echo $film['video']; 
						} ?>
					</div>
					<div class="movies__info">
						<div class="movies__info--header">
							<div class="movies__title">
								<?php _e($film['title'],'cinema'); ?>
							</div>
						</div>
						<div class="movies__info--except">
							<?php echo wp_strip_all_tags($film['except'],'<p>'); ?>
						</div>
						<div class="movies__info--footer">
							<div class="info__inline--start">
								<?php _e($film['rating'],'cinema'); ?>
							</div>
							<div class="info__inline--end">
								<a class="btn btn-alt"
									href="<?php echo esc_url($film['link']['url']); ?>" 
									target="<?php esc_attr_e($film['link']['target'], '_s' ); ?>"
									title="<?php _e($film['link']['title']); ?>">
									<?php _e($film['link']['title']); ?>
								</a>
							</div>
						</div>
					</div>
				</div>
			</li>
	<?php  } 
	}else{ ?>
		<li class="movies__item" data-aos="fade-left" data-aos-duration=1000" data-aos-delay="<?php echo $delay; ?>"> 
			<h2>Nothing Found <?php echo $query ?> </h2>
			<p>Sorry, but nothing matched your search terms. Please try again with some different keywords. </p>
		</li>
	<?php }
	wp_die();
}
	
add_action('wp_ajax_filmfilter', 'cinemall_ajax_film_filter');
add_action('wp_ajax_nopriv_filmfilter', 'cinemall_ajax_film_filter');