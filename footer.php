<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cinema
 */

?>
	<footer id="colophon" class="site-footer">
	 	<div class="block__background-left" data-aos="zoom-in" data-aos-duration="800" data-aos-delay="500">
	 		<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/background/woman-1.png" alt="">
	 	</div>
	 	<div class="footer__container">
	 		<div class="row">
	 			<div class="col-lg-12">
	 				<div class="box box__bordered" data-aos="fade-left" data-aos-duration="800" data-aos-delay="800">
		 				<div class="box section__form box__white">
		 					<?php echo do_shortcode('[contact-form-7 id="125" title="Newsletter"]'); ?>
	 					</div>
	 				</div>
	 			</div>
	 		</div>
		<div class="footer__content ">
			<div class="footer__info box__transparent" data-aos="fade-left" data-aos-duration="800" data-aos-delay="1000">		
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<?php
							$footer_section = get_field('footer_section', 'options');
							 _e($footer_section['content'], 'cinema'); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<?php
							$footer_section = get_field('footer_section', 'options');
							_e($footer_section['contact'],'cinema'); ?>
						</div>
						<ul class="social__list">
							<?php 
							$icons = $footer_section['social_icons'];
							foreach ($icons as $item) : 
								$icon = wp_get_attachment_image(
											$item['icon']['id'],
											'span',
											false,
											array(
												'class' => 'icon__pic',
												'title' => $$item['icon']['title'],
												'alt'   => $item['icon']['alt'],
											)
										);
									?>
								<li class="social__item"><a href="<?php echo esc_url($item['url']); ?>" class="social__link" >
									<?php echo $icon; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h2 data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
								<?php _e($footer_section['map'],'cinema'); ?>
							</h2>
							<div class="maping" data-aos="fade-up" data-aos-duration="800" data-aos-delay="500">
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/background/map.jpg" alt="">
								<?php echo ($footer_section['map_position']); ?></div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer__container">
				<div class="row">
					<div class="col-lg-12"><div class="copyright">
							<a href="#" id="last__title" >
								<h2 class="h2" >
									<?php _e($footer_section['copyright_title'], 'cinema'); ?>
								</h2>
							</a>
							<?php _e($footer_section['copyright'], 'cinema'); ?>			
						</div>
					</div>
				</div>
			</div>
	</footer><!-- #colophon -->
	<div class="brands d-md-none">
		<a href="#" id="last__title1">
			<h2 class="section__title last__title" data-aos="fade-up" data-aos-duration="800" data-aos-delay="500">
				<?php _e($footer_section['copyright_title'], 'cinema'); ?>
			</h2>
		</a>
		<ul class="brand__list">
			<li class="brand__item">
				<div class="brand__item-wrap">
					<a href="#"><img src=<?php echo esc_url(get_template_directory_uri()); ?>/img/brand/assuta.png alt="brand" class="brand__pic"></a>
				</div>
			</li>
			<li class="brand__item">
				<div class="ebrand__item-wrap">
					<a href="#"><img src=<?php echo esc_url(get_template_directory_uri()); ?>/img/brand/planet.png alt="brand" class="brand__pic"></a>
				</div>
			</li>
			<li class="brand__item">
				<div class="brand__item-wrap">
					<a href="#"><img src=<?php echo esc_url(get_template_directory_uri()); ?>/img/brand/train.png alt="brand" class="brand__pic"></a>
				</div>
			</li>
		</ul>
	</div>
</div>
</div>
<!-- #page -->
<?php if (get_field('preloader', 'options')) {?>
<div id="preloader">
  <div id="status">
     <div class="spinner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
  </div>
</div>
<?php } ?>

<?php wp_footer(); ?>

</body>
</html>
