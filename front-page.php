<?php
/**
 * Template Name: Front Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cinema
 */

get_header();

global $is_mobile;

$display = 3;
if ($is_mobile) {
	$display = 1;
}
if (get_field('show_featured')) : 
	$featured_arr = get_field('featured_section');
	?>
 <section class="section__featured">
 	<div class="container-fluid">	
 		<div class="row">
 			<div class="col-lg-12">
 				<div class="section__header">
 					<h2 class="section__title"  data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
 						<?php _e($featured_arr['section_title']); ?> 							
 					</h2>
 				</div> 				
				<div id="featured__slider" class="slick__large"> 
				  <carousel-3d :inverse-scaling="100" :space="600" :minSwipeDistance="500" :display="<?php echo esc_attr($display); ?>" :perspective="0" :controls-visible="true" :width="630" :height="550" >
				  		<?php 
						$carusel = $featured_arr['carusel'];
						foreach ($carusel as$key=> $item) :  ?>
							<slide :index="<?php echo $key; ?>">
								<div class="poligon-child">
							  		<img src="<?php echo esc_url($item['image']['url']);?>" alt="<?php echo esc_url($item['image']['alt']);?>">
							  	</div>
						    </slide>
						   <?php endforeach; ?>
				  </carousel-3d>

				</div>
 			</div>
 		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="section__footer">
					<div class="section__button"  data-aos="fade-up" data-aos-duration="800" data-aos-delay="500">
						<a class="btn btn-alt"
							href="<?php echo esc_url($featured_arr['button_link']['url']); ?>" 
							target="<?php esc_attr_e($featured_arr['button_link']['target'], '_s' ); ?>"
							title="<?php esc_attr_e($featured_arr['button_link']['link-text']); ?>">
							<?php _e($featured_arr['button_caption']); ?></a>
					</div>
				</div>
			</div>
 		</div>
 	</div>

 </section>
 <?php
endif;
//echo do_shortcode(' [wonderplugin_3dcarousel id=1]');
 if (get_field('show_films')) : 
	$films_arr = get_field('section_films');
?>
 <section class="section__films">
 	<div class="block__background-left" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="500">
 		<img src="<?php echo get_template_directory_uri(); ?>/img/background/girl-2.png" alt="girl">
 	</div> 	
 	<div class="block__background-right" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="800">
 		<img src="<?php echo get_template_directory_uri(); ?>/img/background/woman-2.png" alt="woman">
 	</div>
 	<div class="container-fluid">	

 		<div class="row">
 			<div class="col-lg-12">
 				 <div class="section__header">
 					<h2 class="section__title section__title-center"  data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
 						<?php _e($films_arr['section_title']); ?> 							
 					</h2>
 				</div>
 				<div id="films__slider" class="slick-medium">
					
 				 <carousel-3d :inverse-scaling="80"  :space="200" :perspective="0" :display="<?php echo esc_attr($display); ?>" :controls-visible="true" :width="<?php echo esc_attr(($display==1)? '400':'610'); ?>" :height="320" >	 			 
				  		<?php 
						$films = $films_arr['films'];
						foreach ($films as $key => $item) :  ?>
							<slide :index="<?php echo $key; ?>">
								<div class="poligon-child">
									<?php
									$film = get_field('section_film', $item);
								
									if ($film['show_img']) {
										$film_img = $film['img'];
										if ( $film_img ) {
											$img_f = wp_get_attachment_image(
											$film_img,
											'section',
											false,
											array(
												'class' => 'film__imgc',
												'title' => $film_img['title'],
												'alt'   => $film_img['alt'],
											)
										);
										echo $img_f;
										}
									}
									if ($film['show_video']) {
										echo $film['video']; 
									} ?>
										<!-- $image = $film['video'];
				
									if ( $image ) {
										$img = wp_get_attachment_image(
											$image,
											'section',
											false,
											array(
												'class' => $class . 'slick__pic',
												'title' => $image['title'],
												'alt'   => $image['alt'],
											)
										);
										echo $img;
									} ?> -->
							  	</div>
						    </slide>
						<?php endforeach; ?>
				 	</carousel-3d>
				</div>
 			</div>
 		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="section__footer">
					<div class="section__button"  data-aos="fade-up" data-aos-duration="800" data-aos-delay="500">
						<a class="btn btn-alt"
							href="<?php echo esc_url($films_arr['button_link']['url']); ?>" 
							target="<?php esc_attr_e($films_arr['button_link']['target'], '_s' ); ?>"
							title="<?php _e($films_arr['button_link']['link-text']); ?>">
							<?php _e($films_arr['button_caption']); ?></a>
					</div>
				</div>
			</div>
 		</div>
 	</div>
 </section>
<?php endif;  

if (get_field('show__eat')) : 
	$eat_arr = get_field('section_eat');
?>
 <section class="section__eat">
 	<div class="block__background-right block__background-top"  data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="500">
 		<img src="<?php echo get_template_directory_uri(); ?>/img/background/kids.png" alt="">
 	</div>
 	<div class="container-fluid">	
 
 		<div class="row">
 			<div class="col-lg-12">				
 				<div class="section__header">
 					<h2 class="section__title section__title-center"  data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
 						<?php _e($eat_arr['section_title']); ?> 							
 					</h2>
 				</div>

 				<div id="eat__slider" class="slick-small">
					
				 <carousel-3d :inverse-scaling="100" :space="250" :perspective="0" :display="<?php echo esc_attr($display); ?>" :controls-visible="true" :width="460" :height="410" >
				  		<?php 
						$list_eat = $eat_arr['list_eat'];
						foreach ($list_eat as $key => $eat) :  ?>
							<slide :index="<?php echo $key; ?>">
								<div class="poligon-child">
									<?php 
							  		$image = $eat['image'];
				
									if ( $image ) {
										$img = wp_get_attachment_image(
											$image['id'],
											'section',
											false,
											array(
												'class' => $class . 'slick__pic',
												'title' => $image['title'],
												'alt'   => $image['alt'],
											)
										);
										echo $img;
									} ?>
							  	</div>
						    </slide>
						<?php endforeach; ?>
				 	</carousel-3d>
				</div>
 			</div>
 		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="section__footer">
					<div class="section__button"  data-aos="fade-up" data-aos-duration="800" data-aos-delay="500">
						<a class="btn btn-alt"
							href="<?php echo esc_url($eat_arr['button_link']['url']); ?>" 
							target="<?php esc_attr_e($eat_arr['button_link']['target'], '_s' ); ?>"
							title="<?php _e($eat_arr['button_link']['link-text']); ?>">
							<?php _e($eat_arr['button_caption']); ?></a>
					</div>
				</div>
			</div>
 		</div>
 	</div>
</section>
<?php endif; 

if (get_field('show_shop')) : 
	$section_shop = get_field('section_shop');
?>
 <section class="section_shop">
 	<div class="container-fluid">	

 		<div class="row">
 			<div class="col-lg-12">
 				<div class="section__header">
 					<h2 class="section__title section__title-center"  data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">
 						<?php _e($section_shop['section_title']); ?> 							
 					</h2>
 				</div> 				
 				<div id="shop__slider" class="slick-small">

				 <carousel-3d :inverse-scaling="100"   :space="170"  :perspective="0" :display="<?php echo esc_attr($display); ?>" :controls-visible="true" :width="640" :height="400" >
				  	<?php 
					$shop_list = $section_shop['shop_list'];
						foreach ($shop_list as $key => $shop) :  ?>
							<slide :index="<?php echo $key; ?>">
								<div class="poligon-child">
									<?php 
							  		$image = $shop['image'];
				
									if ( $image ) {
										$img = wp_get_attachment_image(
											$image['id'],
											'section',
											false,
											array(
												'class' => $class . 'slick__pic',
												'title' => $image['title'],
												'alt'   => $image['alt'],
											)
										);
										echo $img;
									} ?>
							  	</div>
						    </slide>
						<?php endforeach; ?>
				 	</carousel-3d>
				</div>
 			</div>
 		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="section__footer">
					<div class="section__button"  data-aos="fade-up" data-aos-duration="800" data-aos-delay="500">
						<a class="btn btn-alt"
							href="<?php echo esc_url($section_shop['button_link']['url']); ?>" 
							target="<?php esc_attr_e($eat_section_shop['button_link']['target'], '_s' ); ?>"
							title="<?php _e($section_shop['button_link']['link-text']); ?>">
							<?php _e($section_shop['button_caption']); ?></a>
					</div>
				</div>
			</div>
 		</div>
 	</div>
 </section>
<?php
endif;

get_footer();